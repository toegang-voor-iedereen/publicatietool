{{/*
Copyright VMware, Inc.
SPDX-License-Identifier: APACHE-2.0
*/}}

{{/* vim: set filetype=mustache: */}}
{{/*
Return the proper NGINX image name
*/}}
{{- define "publicatietool.nginx.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.nginx.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper Prometheus metrics image name
*/}}
{{- define "publicatietool.nginx.metrics.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.nginx.metrics.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "publicatietool.nginx.imagePullSecrets" -}}
{{ include "common.images.renderPullSecrets" (dict "images" (list .Values.nginx.image .Values.nginx.metrics.image) "context" $) }}
{{- end -}}

{{/*
Compile all warnings into a single message, and call fail.
*/}}
{{- define "publicatietool.nginx.validateValues" -}}
{{- $messages := list -}}
{{- $messages := append $messages (include "publicatietool.nginx.validateValues.extraVolumes" .) -}}
{{- $messages := without $messages "" -}}
{{- $message := join "\n" $messages -}}

{{- if $message -}}
{{-   printf "\nVALUES VALIDATION:\n%s" $message | fail -}}
{{- end -}}
{{- end -}}

{{/* Validate values of NGINX - Incorrect extra volume settings */}}
{{- define "publicatietool.nginx.validateValues.extraVolumes" -}}
{{- if and (.Values.nginx.extraVolumes) (not .Values.nginx.extraVolumeMounts) -}}
nginx: missing-extra-volume-mounts
    You specified extra volumes but not mount points for them. Please set
    the extraVolumeMounts value
{{- end -}}
{{- end -}}

{{/*
 Create the name of the service account to use
 */}}
{{- define "publicatietool.nginx.serviceAccountName" -}}
{{- if .Values.nginx.serviceAccount.create -}}
    {{ default (include "common.names.fullname" .) .Values.nginx.serviceAccount.name }}-nginx
{{- else -}}
    {{ default "default" .Values.nginx.serviceAccount.name }}-nginx
{{- end -}}
{{- end -}}
