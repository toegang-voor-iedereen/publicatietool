{{- /*
Copyright VMware, Inc.
SPDX-License-Identifier: APACHE-2.0
*/}}

{{- if .Values.nginx.ingress.enabled }}
{{- $nginxServiceName := printf "%s-nginx" (include "common.names.fullname" $) }}
apiVersion: {{ include "common.capabilities.ingress.apiVersion" . }}
kind: Ingress
metadata:
  name: {{ $nginxServiceName }}
  namespace: {{ include "common.names.namespace" . | quote }}
  labels: {{- include "common.labels.standard" ( dict "customLabels" .Values.commonLabels "context" $ ) | nindent 4 }}
  {{- if or .Values.nginx.ingress.annotations .Values.commonAnnotations }}
  {{- $annotations := include "common.tplvalues.merge" ( dict "values" ( list .Values.nginx.ingress.annotations .Values.commonAnnotations ) "context" . ) }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" $annotations "context" $) | nindent 4 }}
  {{- end }}
spec:
  {{- if and .Values.nginx.ingress.ingressClassName (eq "true" (include "common.ingress.supportsIngressClassname" .)) }}
  ingressClassName: {{ .Values.nginx.ingress.ingressClassName | quote }}
  {{- end }}
  rules:
    {{- if .Values.nginx.ingress.hostname }}
    - host: {{ .Values.nginx.ingress.hostname | quote }}
      http:
        paths:
          {{- if .Values.nginx.ingress.extraPaths }}
          {{- toYaml .Values.nginx.ingress.extraPaths | nindent 10 }}
          {{- end }}
          - path: {{ .Values.nginx.ingress.path }}
            {{- if eq "true" (include "common.ingress.supportsPathType" .) }}
            pathType: {{ .Values.nginx.ingress.pathType }}
            {{- end }}
            backend: {{- include "common.ingress.backend" (dict "serviceName" $nginxServiceName "servicePort" (ternary "http" "https" (not $.Values.nginx.containerPorts.https)) "context" $)  | nindent 14 }}
    {{- else if .Values.nginx.ingress.path }}
    - http:
        paths:
          {{- if .Values.nginx.ingress.extraPaths }}
          {{- toYaml .Values.nginx.ingress.extraPaths | nindent 10 }}
          {{- end }}
          - path: {{ .Values.nginx.ingress.path }}
            {{- if eq "true" (include "common.ingress.supportsPathType" .) }}
            pathType: {{ .Values.nginx.ingress.pathType }}
            {{- end }}
            backend: {{- include "common.ingress.backend" (dict "serviceName" $nginxServiceName "servicePort" (ternary "http" "https" (not .Values.nginx.containerPorts.https)) "context" $)  | nindent 14 }}
    {{- end }}
    {{- range .Values.nginx.ingress.extraHosts }}
    - host: {{ .name | quote }}
      http:
        paths:
          - path: {{ default "/" .path }}
            {{- if eq "true" (include "common.ingress.supportsPathType" $) }}
            pathType: {{ default "ImplementationSpecific" .pathType }}
            {{- end }}
            backend: {{- include "common.ingress.backend" (dict "serviceName" $nginxServiceName "servicePort" (ternary "http" "https" (not $.Values.nginx.containerPorts.https)) "context" $) | nindent 14 }}
    {{- end }}
    {{- if .Values.nginx.ingress.extraRules }}
    {{- include "common.tplvalues.render" (dict "value" .Values.nginx.ingress.extraRules "context" $) | nindent 4 }}
    {{- end }}
  {{- if or (and .Values.nginx.ingress.tls (or (include "common.ingress.certManagerRequest" ( dict "annotations" .Values.nginx.ingress.annotations )) .Values.nginx.ingress.selfSigned (not (empty .Values.nginx.ingress.secrets)))) .Values.nginx.ingress.extraTls }}
  tls:
    {{- if and .Values.nginx.ingress.tls (or (include "common.ingress.certManagerRequest" ( dict "annotations" .Values.nginx.ingress.annotations )) .Values.nginx.ingress.selfSigned (not (empty .Values.nginx.ingress.secrets))) }}
    - hosts:
        - {{ .Values.nginx.ingress.hostname | quote }}
        {{- if or (.Values.nginx.ingress.tlsWwwPrefix) (eq (index .Values.nginx.ingress.annotations "publicatietool.nginx.ingress.kubernetes.io/from-to-www-redirect") "true" ) }}
        - {{ printf "www.%s" (tpl .Values.nginx.ingress.hostname $) | quote }}
        {{- end }}
      secretName: {{ printf "%s-tls" .Values.nginx.ingress.hostname }}
    {{- end }}
    {{- if .Values.nginx.ingress.extraTls }}
    {{- include "common.tplvalues.render" (dict "value" .Values.nginx.ingress.extraTls "context" $) | nindent 4 }}
    {{- end }}
  {{- end }}
{{- end }}
